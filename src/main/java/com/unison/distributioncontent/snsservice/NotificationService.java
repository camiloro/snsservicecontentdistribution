package com.unison.distributioncontent.snsservice;

import java.net.URI;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.unison.distributioncontent.snsservice.dto.ContentMessage;



@RestController
public class NotificationService {

	@GetMapping(path="/id/{id}")
	public ResponseEntity<Object> addUser(@PathVariable  String id) {
		ContentMessage messageToSend= PushNotificationService.push(id);
		URI location= ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(messageToSend.getId()).toUri();
		return ResponseEntity.created(location).build();
	}
}

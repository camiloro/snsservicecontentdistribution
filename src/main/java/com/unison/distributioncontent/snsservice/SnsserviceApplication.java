package com.unison.distributioncontent.snsservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SnsserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SnsserviceApplication.class, args);
	}

}
